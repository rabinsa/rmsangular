import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

import { AddLocationComponent } from './components/add-location/add-location.component';
import { LocationListComponent } from './location-list/location-list.component';
import { LocationModule } from './location.module';

export const ROUTES: Routes = [
  {
      path: "",
      data: {
          title: "Location",
          breadcrumbs: [
              {
                  text: "Location",
                  active: true,
              },
          ],
      } as SBRouteData,
      canActivate: [],
      component:LocationListComponent,
      children: [
          { path: "add", component: AddLocationComponent }
          // { path: 'edit/:id', component: AddEmployeeComponent },
      ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    LocationModule,
    RouterModule.forChild(ROUTES)
  ]
})
export class LocationRoutingModule { }
