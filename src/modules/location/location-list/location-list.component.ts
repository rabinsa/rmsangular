import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '@modules/_animations/fade-in.animation';
import { finalize } from 'rxjs/internal/operators/finalize';

import { LocationService } from '../services/location.service';

@Component({
  selector: 'sb-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.scss'],

  // make fade in animation available to this component
  animations: [fadeInAnimation],
  // attach the fade in animation to the host (root) element of this component
  // tslint:disable-next-line: no-host-metadata-property
  host: { '[@fadeInAnimation]': '' },
})
export class LocationListComponent implements OnInit {
  currentEvent = 'start do something';
  config = {
    showActionButtons: true,
    showAddButtons: true,
    showRenameButtons: true,
    showDeleteButtons: true,
    showRootActionButtons: true, // set false to hide root action buttons.
    enableExpandButtons: true,
    enableDragging: false,
    rootTitle: 'Locations',
    validationText: 'Enter valid location',
    minCharacterLength: 3,
    setItemsAsLinks: false,
    setFontSize: 16,
    setIconSize: 16
  };


  locationTree = [];
  constructor(private _locationService: LocationService) {
  }

  ngOnInit(): void {
    this.getAllLocationTree();
  }

  getAllLocationTree() {
    this._locationService.getAllLocationTree().subscribe(res => {
      this.locationTree = res;
    })
  }

  onDragStart(event: any) {
    this.currentEvent = 'on drag start';
    console.log(event);
  }
  onDrop(event: any) {
    this.currentEvent = 'on drop';
    console.log(event);
  }
  onAllowDrop(event: any) {
    this.currentEvent = 'on allow drop';
  }
  onDragEnter(event: any) {
    this.currentEvent = 'on drag enter';
  }
  onDragLeave(event: any) {
    this.currentEvent = 'on drag leave';
  }
  onAddItem(event: any) {
    this.currentEvent = 'on add item';
    console.log(event);
  }
  onStartRenameItem(event: any) {
    this.currentEvent = 'on start edit item';
  }

  onFinishRenameItem(event: any) {
    this.currentEvent = 'on finish edit item';
    if (event.parent === 'root') {
      this.addLocation(event.element.name, null);
    } else if (event.parent.id) {
      this.addLocation(event.element.name, event.parent.id);
    }
  }

  onStartDeleteItem(event: any) {
    console.log('start delete');
    this.currentEvent = 'on start delete item';
  }
  onFinishDeleteItem(event: any) {
    console.log('finish delete');
    this.currentEvent = 'on finish delete item';
  }
  onCancelDeleteItem(event: any) {
    console.log('cancel delete');
    this.currentEvent = 'on cancel delete item';
  }

  addLocation(locationName: string, parentId: any) {
    const data = {
      name: locationName,
      parentId
    };
    this._locationService.saveLocation(data).pipe(finalize(() => {
      this.getAllLocationTree();
    })).subscribe(res => {
    })
  }





}
