import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { ApiService } from "@common/services/api.service";
import { fadeInAnimation } from "@modules/_animations/fade-in.animation";

@Component({
    selector: "sb-dashboard",
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: "./dashboard.component.html",
    styleUrls: ["dashboard.component.scss"],
    // make fade in animation available to this component
    animations: [fadeInAnimation],
    // attach the fade in animation to the host (root) element of this component
    // tslint:disable-next-line: no-host-metadata-property
    host: { "[@fadeInAnimation]": "" },
})
export class DashboardComponent implements OnInit {
    processDate: any;
    firstWDates: any = [];
    firstWeekStart = 1;
    secondWDates: any = [];
    thirdWDates: any = [];
    forthWDates: any = [];
    fifthWDates: any = [];
    sixthWDates: any = [];
    lastDayInNumber: number;

    lastDate: any;
    firstDate: any;

    constructor(private apiSvr: ApiService, private router: Router) {
        this.initPage(router);
        this.lastDayInNumber = 0;
    }

    ngOnInit() {
       // this.getCreateFirstWeek();
    }

    // getCreateFirstWeek() {
    //     // const date = new Date(2019, 5, 1);
    //     const date = new Date();
    //     this.firstDate = new Date(date.getFullYear(), date.getMonth(), 1);
    //     this.lastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    //     this.lastDayInNumber = parseInt(this.lastDate.toString().split(" ")[2], 10);

    //     this.processDate = this.firstDate.toString();
    //     let dayNumber = this.firstDate.getDay();
    //     this.firstWDates = [];
    //     if (dayNumber === 0) {
    //         for (let i = 0; i <= 6; i++) {
    //             dayNumber++;
    //             this.firstWDates.push(dayNumber);
    //         }
    //     } else {
    //         for (let i = 0; i < dayNumber; i++) {
    //             this.firstWDates.push(0);
    //         }
    //         let d = 0;
    //         for (let i = dayNumber; i <= 6; i++) {
    //             d++;
    //             this.firstWDates.push(d);
    //         }
    //     }
    //     this.createSecondWeek();
    // }

    // createSecondWeek() {
    //     let sndWeekStartDayInNum = this.getListItemFormArray(this.firstWDates);
    //     for (let i = 0; i <= 6; i++) {
    //         sndWeekStartDayInNum++;
    //         this.secondWDates.push(sndWeekStartDayInNum);
    //     }
    //     this.createThirdWeek();
    // }

    // createThirdWeek() {
    //     let thirdWeekStartDayInNum = this.getListItemFormArray(this.secondWDates);
    //     for (let i = 0; i <= 6; i++) {
    //         thirdWeekStartDayInNum++;
    //         this.thirdWDates.push(thirdWeekStartDayInNum);
    //     }
    //     this.createForthWeek();
    // }

    // createForthWeek() {
    //     let forthWeekStartDayInNum = this.getListItemFormArray(this.thirdWDates);
    //     for (let i = 0; i <= 6; i++) {
    //         forthWeekStartDayInNum++;
    //         this.forthWDates.push(forthWeekStartDayInNum);
    //     }
    //     this.createFifthWeek();
    // }

    // createFifthWeek() {
    //     let fifthWeekStartDayInNum = this.getListItemFormArray(this.forthWDates);
    //     for (let i = 0; i <= 6; i++) {
    //         fifthWeekStartDayInNum++;
    //         if (fifthWeekStartDayInNum <= this.lastDayInNumber) {
    //             this.fifthWDates.push(fifthWeekStartDayInNum);
    //         }
    //     }

    //     const fifthWLastValue = this.getListItemFormArray(this.fifthWDates);
    //     if (fifthWLastValue === this.lastDayInNumber) {
    //         this.lastProcess(this.fifthWDates);
    //     } else {
    //         this.createSixthWeek();
    //     }
    // }

    // createSixthWeek() {
    //     let sixthWeekStartDayInNum = this.getListItemFormArray(this.fifthWDates);
    //     if (this.lastDayInNumber >= sixthWeekStartDayInNum) {
    //         for (let i = sixthWeekStartDayInNum; i < this.lastDayInNumber; i++) {
    //             sixthWeekStartDayInNum++;
    //             this.sixthWDates.push(sixthWeekStartDayInNum);
    //         }
    //     }
    //     const sisthWLastValue = this.getListItemFormArray(this.sixthWDates);
    //     if (sisthWLastValue === this.lastDayInNumber) {
    //         this.lastProcess(this.sixthWDates);
    //     }
    // }

    // lastProcess(lastWeekArray: any) {
    //     const lastDayNumber = this.lastDate.getDay();
    //     for (let i = lastDayNumber; i < 6; i++) {
    //         lastWeekArray.push(0);
    //     }
    // }

    // getListItemFormArray(array: []): any {
    //     return array[array.length - 1];
    // }

    getData(value: any) {}

    initPage(router: any) {
        router.events.subscribe(() => {
            document.body.style.overflow = "visible";
        });
    }
}
