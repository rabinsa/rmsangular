/* tslint:disable: ordered-imports*/
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

/* Modules */
import { AppCommonModule } from "@common/app-common.module";
import { NavigationModule } from "@modules/navigation/navigation.module";



/* Containers */
import * as dashboardContainers from "./containers";

/* Guards */
import * as dashboardGuards from "./guards";

/* Services */
import * as dashboardServices from "./services";
import { AttndDetailsComponent } from "./components/attnd-details/attnd-details.component";
import { DashboardCardsComponent } from './components/dashboard-cards/dashboard-cards.component';

@NgModule({
    imports: [CommonModule, RouterModule, ReactiveFormsModule, FormsModule, AppCommonModule, NavigationModule],
    providers: [...dashboardServices.services, ...dashboardGuards.guards],
    declarations: [...dashboardContainers.containers,DashboardCardsComponent, AttndDetailsComponent],
    exports: [...dashboardContainers.containers,DashboardCardsComponent],
})
export class DashboardModule {}
