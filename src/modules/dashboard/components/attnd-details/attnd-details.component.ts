import { AfterViewInit, Component, HostListener, OnInit } from "@angular/core";
import { slideInOutAnimation } from "@modules/_animations/slide-in-out.animation";

@Component({
    selector: "sb-attnd-details",
    templateUrl: "./attnd-details.component.html",
    styleUrls: ["./attnd-details.component.scss"],
    // make slide in/out animation available to this component
    animations: [slideInOutAnimation],
    // attach the slide in/out animation to the host (root) element of this component
    // tslint:disable-next-line: no-host-metadata-property
    host: { "[@slideInOutAnimation]": "" },
})
export class AttndDetailsComponent implements OnInit, AfterViewInit {
    public screenWidth: any;
    public screenHeight: any;
    public height: any;

    constructor() {
        this.initPage();
    }

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        document.body.style.overflow = "hidden";
    }

    initPage() {
        const scrnHeight = window.innerHeight;
        this.screenHeight = scrnHeight - (56 + 50);
    }
}
