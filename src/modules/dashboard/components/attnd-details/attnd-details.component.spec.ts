import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { AttndDetailsComponent } from "./attnd-details.component";

describe("AttndDetailsComponent", () => {
    let component: AttndDetailsComponent;
    let fixture: ComponentFixture<AttndDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AttndDetailsComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AttndDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
