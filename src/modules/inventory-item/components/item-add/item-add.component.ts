import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '@common/services/notification.service';
import { ItemService } from '@modules/inventory-item/services/item.service';
import { slideInOutAnimation } from '@modules/_animations/slide-in-out.animation';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'sb-item-add',
  templateUrl: './item-add.component.html',
  styleUrls: ['./item-add.component.scss'],

  // make slide in/out animation available to this component
  animations: [slideInOutAnimation],
  // attach the slide in/out animation to the host (root) element of this component
  // tslint:disable-next-line: no-host-metadata-property
  host: { '[@slideInOutAnimation]': '' },
})
export class ItemAddComponent implements OnInit {
  paymentPerc = 70;
  itemForm: FormGroup;
  submitted = false;
  screenHeight: any;
  public loading: boolean;
  btnText: any;

  itemModel: any = {};
  dynamicArray: Array<any> = [];
  newDynamic: any = {};
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private _notifyService: NotificationService,
    private ref: ChangeDetectorRef,
    private _itemSvr: ItemService,
  ) {
    this.loading = false;
    this.btnText = '';
    this.initPage();
    setTimeout(() => {
     // this.paymentPerc = 70;
    }, 1000);
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(routeParams => {
      this.getItemById(routeParams.id);
    })
    this.newDynamic = { BarCode: "", SerialNumber: "", IMEI: "", MacAddress: "" };
    this.dynamicArray.push(this.newDynamic);
    this.initItemForm();
  }

  addRow(index: any) {
    this.newDynamic = { BarCode: "", SerialNumber: "", IMEI: "", MacAddress: "" };
    this.dynamicArray.push(this.newDynamic);
    // this.toastr.success('New row added successfully', 'New Row');
    console.log(this.dynamicArray);
    return true;
  }

  deleteRow(index: any) {
    if (this.dynamicArray.length === 1) {
      // this.toastr.error("Can't delete the row when there is only one row", 'Warning');
      return false;
    } else {
      this.dynamicArray.splice(index, 1);
      // this.toastr.warning('Row deleted successfully', 'Delete row');
      return true;
    }
  }

  getItemById(id: any) {
    if (id) {
      this.btnText = 'Save';
      this._itemSvr.getItemById(id).subscribe(res => {
        if (res) {
          this.itemForm.patchValue(res);
        }
      })
    }
    else {
      this.btnText = 'Create';
    }
  }
  initItemForm() {
    this.itemForm = this.formBuilder.group({
      id: [''],
      itemCode: ['', Validators.required],
      itemName: ['', Validators.required],
      category: [null],
      description: [null],
      itemPrice: [null],
      quantity: [''],
      createdBy: ['']
    })
  }
  get f() {
    return this.itemForm.controls;
  }
  // onSubmit() {
  //   this.submitted = true;
  //   this.loading = true;
  //   // stop ere if form is invalid
  //   if (this.itemForm.invalid) {
  //     const invalid = [];
  //     const controls = this.itemForm.controls;
  //     for (const name in controls) {
  //       if (controls[name].invalid) {
  //         invalid.push(name);
  //       }
  //     }
  //     console.log(invalid);
  //     this.loading = false;
  //     return;
  //   }

  //   let rquestObservable = new Observable<any>();
  //   if (this.itemForm.value.id) {
  //     rquestObservable = this._itemSvr
  //       .itemUpdate(this.itemForm.value);
  //   } else {
  //     rquestObservable = this._itemSvr
  //       .itemCreate(this.itemForm.value);
  //   }
  //   rquestObservable.pipe(
  //     finalize(() => {
  //       this.loading = false;
  //       this.ref.detectChanges();
  //     })
  //   )
  //     .subscribe(
  //       res => {
  //         this._notifyService.showSuccessWithTimeout(
  //           'Saved successfully !!',
  //           'Notification',
  //           3000
  //         );
  //         this.router.navigate(['/itemlist']);
  //       },
  //       error => { }
  //     );
  // }

  onSubmit() {
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.itemModel));
    // this._locationService.saveLocation(this.model).pipe(finalize(() => {
    // })).subscribe(res => {
    // })
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit(): void {
    document.body.style.overflow = 'hidden';
  }
  initPage() {
    const scrnHeight = window.innerHeight;
    this.screenHeight = scrnHeight - (56 + 50);
  }

}
