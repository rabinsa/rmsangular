import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

import { ItemAddComponent } from './components/item-add/item-add.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemModule } from './item.module';

export const ROUTES: Routes = [
  {
      path: "",
      data: {
          title: "Inventory - Item",
          breadcrumbs: [
              {
                  text: "List Item",
                  active: true,
              },
          ],
      } as SBRouteData,
      canActivate: [],
      component: ItemListComponent,
      children: [
           { path: "add", component: ItemAddComponent },
          // { path: 'edit/:id', component: AddVendorComponent },
      ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ItemModule,
    RouterModule.forChild(ROUTES)
  ]
})
export class ItemRoutingModule { }
