import { ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { SBSortableHeaderDirective, SortEvent } from '@common/directives';
import { fadeInAnimation } from '@modules/_animations/fade-in.animation';
import { Observable } from 'rxjs';

import { ItemService } from '../services/item.service';

@Component({
  selector: 'sb-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],

  // make fade in animation available to this component
  animations: [fadeInAnimation],
  // attach the fade in animation to the host (root) element of this component
  // tslint:disable-next-line: no-host-metadata-property
  host: { '[@fadeInAnimation]': '' },
})
export class ItemListComponent implements OnInit {

   pageSize = 5;
   items$!: Observable<any>;
   total$!: Observable<number>;
   sortedColumn!: string;
   // tslint:disable-next-line: prettier
   sortedDirection!: string;

   @ViewChildren(SBSortableHeaderDirective) headers!: QueryList<SBSortableHeaderDirective>;

   constructor(
     public itemService: ItemService,
     private changeDetectorRef: ChangeDetectorRef,
     private router: Router
   ) {
     this.initPage(router);

   }

   ngOnInit(): void {
     this.itemService.pageSize = this.pageSize;
     this.items$ = this.itemService.items$
     this.total$ = this.itemService.total$;
   }

   reSetGrid() {
     this.itemService.callItemData();
   }

   initPage(router: any) {
     router.events.subscribe(() => {
       document.body.style.overflow = 'visible';
     });
   }

   onSort({ column, direction }: SortEvent) {
     this.sortedColumn = column;
     this.sortedDirection = direction;
     this.itemService.sortColumn = column;
     this.itemService.sortDirection = direction;
     this.changeDetectorRef.detectChanges();
   }

}
