import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

import { ItemAddComponent } from './components/item-add/item-add.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemService } from './services/item.service';




@NgModule({
  providers: [DecimalPipe,ItemService],
  declarations: [ItemListComponent, ItemAddComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    AppCommonModule,
    NavigationModule],
})
export class ItemModule { }
