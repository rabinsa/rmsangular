import { CommonModule, DecimalPipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { AppCommonModule } from "@common/app-common.module";
import { NavigationModule } from "@modules/navigation/navigation.module";

import { AllEmployeeComponent } from "./all-employee/all-employee.component";
import { AddEmployeeComponent } from "./components/add-employee/add-employee.component";
import { EmployeeService } from "./services/employee.service";

@NgModule({
    providers: [DecimalPipe, EmployeeService],
    declarations: [AllEmployeeComponent, AddEmployeeComponent],
    imports: [CommonModule, RouterModule, ReactiveFormsModule, FormsModule, AppCommonModule, NavigationModule],
})
export class AllEmployeeModule {}
