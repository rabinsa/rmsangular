import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnInit,
    QueryList,
    ViewChildren,
} from '@angular/core';
import { Router } from '@angular/router';
import { SBSortableHeaderDirective, SortEvent } from '@common/directives/sortable.directive';
import { Observable } from 'rxjs';

import { fadeInAnimation } from '../../_animations/index';
import { EmployeeService } from '../services/employee.service';

@Component({
    selector: 'sb-all-employee',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './all-employee.component.html',
    styleUrls: ['./all-employee.component.scss'],
    // make fade in animation available to this component
    animations: [fadeInAnimation],
    // attach the fade in animation to the host (root) element of this component
    // tslint:disable-next-line: no-host-metadata-property
    host: { '[@fadeInAnimation]': '' },
})
export class AllEmployeeComponent implements OnInit {
    @Input() pageSize = 2;

    employees$!: Observable<any>;
    total$!: Observable<number>;
    sortedColumn!: string;
    // tslint:disable-next-line: prettier
    sortedDirection!: string;

    @ViewChildren(SBSortableHeaderDirective) headers!: QueryList<SBSortableHeaderDirective>;

    constructor(
        public employeeService: EmployeeService,
        private changeDetectorRef: ChangeDetectorRef,
        private router: Router
    ) {
        this.initPage(router);
    }

    ngOnInit() {
        this.employeeService.pageSize = this.pageSize;
        this.employees$ = this.employeeService.employees$;
        this.total$ = this.employeeService.total$;
    }

    onSort({ column, direction }: SortEvent) {
        this.sortedColumn = column;
        this.sortedDirection = direction;
        this.employeeService.sortColumn = column;
        this.employeeService.sortDirection = direction;
        this.changeDetectorRef.detectChanges();
    }

    reSetGrid() {
        this.employeeService.callEmplpyeeData();
    }

    initPage(router: any) {
        router.events.subscribe(() => {
            document.body.style.overflow = 'visible';
        });
    }
}
