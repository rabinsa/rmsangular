import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SBRouteData } from "@modules/navigation/models";

import { AllEmployeeModule } from "./all-employee.module";
import { AllEmployeeComponent } from "./all-employee/all-employee.component";
import { AddEmployeeComponent } from "./components/add-employee/add-employee.component";

export const ROUTES: Routes = [
    {
        path: "",
        data: {
            title: "Employee - Create",
            breadcrumbs: [
                {
                    text: "All Employee",
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: AllEmployeeComponent,
        children: [
            { path: "add", component: AddEmployeeComponent },
            { path: 'edit/:id', component: AddEmployeeComponent },
        ],
    },
];

@NgModule({
    declarations: [],
    imports: [CommonModule, AllEmployeeModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AllEmployeeRoutingModule {}
