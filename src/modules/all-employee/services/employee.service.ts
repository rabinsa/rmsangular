import { DecimalPipe } from "@angular/common";
import { Injectable, PipeTransform } from "@angular/core";
import { SortDirection } from '@common/directives/sortable.directive';
import { ApiService } from "@common/services/api.service";
import { BehaviorSubject, Observable, of, Subject } from "rxjs";
import {  switchMap, tap } from "rxjs/operators";



interface State {
    page: number;
    pageSize: number;
    searchTerm: string;
    sortColumn: string;
    sortDirection: SortDirection;
}

function compare(v1: number | string, v2: number | string) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

@Injectable({
    providedIn: "root",
})
export class EmployeeService {
    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _employees$ = new BehaviorSubject<any>([]);
    private _total$ = new BehaviorSubject<number>(0);

    private _state: State = {
        page: 1,
        pageSize: 2,
        searchTerm: "",
        sortColumn: "",
        sortDirection: "",
    };

    constructor(private pipe: DecimalPipe, private apiSvr: ApiService) {
        this.callEmplpyeeData();
    }

    callEmplpyeeData() {
        this.resetState();
        this._search$
            .pipe(
                tap(() => this._loading$.next(true)),
                switchMap(() => this._searchAllEmployee()),
                tap(() => this._loading$.next(false))
            )
            .subscribe((result) => {
                if (result && result.data) {
                    this._employees$.next(result.data);
                    this._total$.next(result.count);
                } else {
                    this._employees$.next([]);
                    this._total$.next(1);
                }
            }, _error => {
                this._employees$.next([]);
                this._total$.next(1);
                this.resetState();
            });

        this._search$.next();
    }

    resetState() {
        this._state = {
            page: 1,
            pageSize: 2,
            searchTerm: "",
            sortColumn: "",
            sortDirection: "",
        };
    }

    get employees$() {
        return this._employees$.asObservable();
    }
    get total$() {
        return this._total$.asObservable();
    }
    get loading$() {
        return this._loading$.asObservable();
    }
    get page() {
        return this._state.page;
    }
    set page(page: number) {
        this._set({ page });
    }
    get pageSize() {
        return this._state.pageSize;
    }
    set pageSize(pageSize: number) {
        this._set({ pageSize });
    }
    get searchTerm() {
        return this._state.searchTerm;
    }
    set searchTerm(searchTerm: string) {
        this._set({ searchTerm });
    }
    set sortColumn(sortColumn: string) {
        this._set({ sortColumn });
    }
    set sortDirection(sortDirection: SortDirection) {
        this._set({ sortDirection });
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _searchAllEmployee(): Observable<any> {
        const sendToObj = {
            SearchText: this._state.searchTerm,
            PageSize: this._state.pageSize,
            PageNo: this._state.page,
            OrderByColumn: this._state.sortColumn || 'firstName',
            OrderByDirection: this._state.sortDirection || 'desc'
        };
        return this.apiSvr.get("/Employee", sendToObj);
    }

    employeeCreate(empData: any): Observable<any> {
        return this.apiSvr.post("/employee", empData);
    }

    employeeUpdate(empData: any): Observable<any> {
        return this.apiSvr.put("/employee", empData);
    }

    getEmployeeById(id: any) {
        return this.apiSvr.get(`/Employee/${id}`);
    }

}
