import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import{FormBuilder,FormGroup, Validators} from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '@common/services/notification.service';
import { slideInOutAnimation } from '@modules/_animations';
import { Observable } from 'rxjs/internal/Observable';
import { finalize } from 'rxjs/operators';

import { VendorService } from '../services/vendor.service';

@Component({
  selector: 'sb-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.scss'],

   // make slide in/out animation available to this component
   animations: [slideInOutAnimation],
   // attach the slide in/out animation to the host (root) element of this component
   // tslint:disable-next-line: no-host-metadata-property
   host: { '[@slideInOutAnimation]': '' },
})
export class AddVendorComponent implements OnInit {
  vendorForm: FormGroup;
  submitted = false;
  screenHeight: any;
  public loading: boolean;
  btnText: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private _vndrSvr: VendorService,
    private _notifyService: NotificationService,
    private ref: ChangeDetectorRef,



  ) {
    this.loading = false;
    this.btnText = '';
    this.initPage();
   }


  ngOnInit(): void {
    this.activeRoute.params.subscribe(routeParams => {
      this.getVendorById(routeParams.id);
    })
    this.initVendorForm();
  }

  getVendorById(id: any) {
    if (id) {
      this.btnText = 'Save';
      this._vndrSvr.getVendorById(id).subscribe(res => {
        if (res) {
          this.vendorForm.patchValue(res);
        }
      })
    }
    else {
      this.btnText = 'Create';
    }
  }
  initVendorForm() {
    this.vendorForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      contactName: [''],
      emailAddress: [null],
      phoneNumber: [null],
      officeNumber:[null],
      address: [''],
      website: ['']
  })
  }
  get f() {
    return this.vendorForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    this.loading = true;
    // stop ere if form is invalid
    if (this.vendorForm.invalid) {
      const invalid = [];
      const controls = this.vendorForm.controls;
      for (const name in controls) {
        if (controls[name].invalid) {
          invalid.push(name);
        }
      }
      console.log(invalid);
      this.loading = false;
      return;
    }

    let rquestObservable = new Observable<any>();
    if (this.vendorForm.value.id) {
      rquestObservable = this._vndrSvr
        .vendorUpdate(this.vendorForm.value);
    } else {
      rquestObservable = this._vndrSvr
        .vendorCreate(this.vendorForm.value);
    }

    // this._empSvr
    //     .employeeCreate(this.employeeForm.value)
    rquestObservable.pipe(
      finalize(() => {
        this.loading = false;
        this.ref.detectChanges();
      })
    )
      .subscribe(
        res => {
          this._notifyService.showSuccessWithTimeout(
            'Saved successfully !!',
            'Notification',
            3000
          );
          this.router.navigate(['/vendors']);
        },
        error => { }
      );
  }
  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit(): void {
    document.body.style.overflow = 'hidden';
  }

  initPage() {
    const scrnHeight = window.innerHeight;
    this.screenHeight = scrnHeight - (56 + 50);
  }
}
